﻿using Lte.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lte.Report
{
    public partial class ReportWindow : Form
    {
        private IStorage _storage;
        private IEnumerable<IPlot> _plots;

        public ReportWindow()
            : this(Enumerable.Empty<IPlot>(), null, "Not connected")
        {
        }

        public ReportWindow(IEnumerable<IPlot> plots, IStorage storage, string connectionString)
        {
            _plots = plots;
            _storage = storage;

            InitializeComponent();
            InitializeComboBox();
        }

        private void InitializeComboBox()
        {
            foreach (var plot in _plots)
                plotComboBox.Items.Add(plot);
        }

        private void plotComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var plot = (IPlot)plotComboBox.SelectedItem;
            var plotter = new OxyPlotBuilder();
            
            plot.Draw(plotter, _storage);
            plotView.Model = plotter.GetModel();
        }

        private void dumpAllButton_Click(object sender, EventArgs e)
        {
            foreach (var plot in _plots)
            {
                var baseFileName = Pathize(plot.ToString());

                using (var builder = new PythonPlotBuilder(baseFileName + ".py", baseFileName + ".csv"))
                    plot.Draw(builder, _storage);
            }
        }

        private string Pathize(string p)
        {
            var invalid = Path.GetInvalidFileNameChars();
            var builder = new StringBuilder();
            foreach (var character in p.ToCharArray())
            {
                if (invalid.Contains(character) || Char.IsWhiteSpace(character))
                    builder.Append('_');
                else
                    builder.Append(character);
            }

            return builder.ToString();
        }
    }
}
