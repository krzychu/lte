﻿using Lte.Report;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.ReportTest
{
    public static class PythonTest
    {
        public static void TestPlot()
        {
            using (var builder = new PythonPlotBuilder("script.py", "script_data.csv"))
            { 
                double[] xs = Enumerable.Range(0, 1000).Select(x => (double)x / 100).ToArray();
                double[] ys = xs.Select(x => Math.Sin(x)).ToArray();

                builder.Plot(xs, ys, PlotStyle.Line, Color.Red, "sin");
            }
        }
    }
}
