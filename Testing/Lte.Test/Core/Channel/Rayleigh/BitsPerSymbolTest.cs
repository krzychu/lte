﻿using Lte.Core.Channel.EncodedRayleigh;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Core.Channel.Rayleigh
{
    [TestClass]
    public class BitsPerSymbolTest
    {
        [TestMethod]
        public void ReturnsBiggestPossibleBps()
        {
            Assert.AreEqual(0, BitsPerSymbol.GetBitsPerSymbol(0));
            Assert.AreEqual(4.8, BitsPerSymbol.GetBitsPerSymbol(200));
            Assert.AreEqual(0.5, BitsPerSymbol.GetBitsPerSymbol(1.3));
        }
    }
}
