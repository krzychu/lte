﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.ProportionalFair
{
    public class ProportionalFairSettings : ISchedulerSettings
    {
        public string Name
        {
            get { return "ProportionalFair"; }
        }

        public double Tau { get; set; }

        public IScheduler GetScheduler(SimulationContext context)
        {
            return new ProportionalFairScheduler(context, Tau);
        }
    }
}
