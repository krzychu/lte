﻿using Lte.Core.Channel.EncodedRayleigh;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.SimpleRayleigh
{
    public class SimpleRayleighSettings : IChannelSettings
    {
        public string Name
        {
            get { return "Simple Rayleigh"; }
        }

        public double[] DbMeans { get; set; }
        public double MaxDbSnr { get; set; }
        public double BandwidthHertz { get; set; }
        public double SchedulingIntervalSeconds { get; set; }

        public IChannel GetChannel(SimulationContext context)
        {
            return new SimpleRayleighChannel(
                DbMeans, 
                SchedulingIntervalSeconds, 
                BandwidthHertz, 
                MaxBitsPerSchedulingInterval,
                context.Seed);
        }

        public double MaxBitsPerSchedulingInterval
        {
            get 
            {
                var maxLinearSnr = Util.ToLinear(MaxDbSnr);
                return Shannon.GetBitsPerSecond(BandwidthHertz, maxLinearSnr) * SchedulingIntervalSeconds;
            }
        }

        public static SimpleRayleighSettings FromUserClasses(
            double bandwidthHertz, 
            double schedulingInvervalSeconds, 
            double maxDbSNR, 
            int poorCount, 
            int averageCount, 
            int goodCount)
        {
            double maxAchievableBps = Shannon.GetBitsPerSecond(
                bandwidthHertz, Util.ToLinear(maxDbSNR));

            double poorSnr = Util.ToDb(Shannon.GetLinearSnr(bandwidthHertz, 0.3 * maxAchievableBps));
            double averageSnr = Util.ToDb(Shannon.GetLinearSnr(bandwidthHertz, 0.6 * maxAchievableBps));
            double goodSnr = Util.ToDb(Shannon.GetLinearSnr(bandwidthHertz, 0.8 * maxAchievableBps));

            var means = Enumerable.Repeat(poorSnr, poorCount)
                .Concat(Enumerable.Repeat(averageSnr, averageCount))
                .Concat(Enumerable.Repeat(goodSnr, goodCount))
                .ToArray();

            return new SimpleRayleighSettings()
            {
                BandwidthHertz = bandwidthHertz,
                SchedulingIntervalSeconds = schedulingInvervalSeconds,
                DbMeans = means,
                MaxDbSnr = maxDbSNR
            };
        }
    }
}
