﻿using Lte.Core.Channel;
using Lte.Core.Model;
using Lte.Core.Scheduler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core
{
    public class SimulationRunner
    {
        public const string ChannelRateKpi = "ChannelRate";
        public const string TransmittedKpi = "TransmittedAmount";

        private IStorage _storage;

        public SimulationRunner(IStorage storage)
        {
            _storage = storage;
        }

        public void Run(SimulationSettings settings)
        {
            var simulation = settings.GetSimulation();
            _storage.AddSimulation(simulation);

            foreach (var rep in Enumerable.Range(0, simulation.RepetitionCount))
            { 
                var execution = new Execution()
                {
                    Seed = rep
                };
                simulation.Executions.Add(execution);
                _storage.Flush();

                var context = new SimulationContext()
                {
                    Seed = execution.Seed,
                    UserCount = simulation.UserCount,
                    Duration = simulation.Duration
                };

                
                var scheduler = settings.SchedulerSettings.GetScheduler(context);
                var channel = settings.ChannelSettings.GetChannel(context);

                double[][] channelRate = CreateStateArray(simulation);
                double[][] transmissionRate = CreateStateArray(simulation);

                foreach (var time in Enumerable.Range(0, simulation.Duration))
                {
                    context.Time = time;
                    var rates = channel.GetChannelRates();
                    var selected = scheduler.SelectActiveUser(rates);

                    foreach (var user in Enumerable.Range(0, rates.Length))
                    {
                        double transmitted = (user == selected) ? rates[user] : 0;
                        channelRate[user][time] = rates[user];
                        transmissionRate[user][time] = transmitted;
                    }
                }

                foreach(var user in Enumerable.Range(0, simulation.UserCount))
                {
                    _storage.SetKpiValues(execution.Id, ChannelRateKpi, user, channelRate[user]);
                    _storage.SetKpiValues(execution.Id, TransmittedKpi, user, transmissionRate[user]);
                }
            }

            _storage.Flush();
        }

        private static double[][] CreateStateArray(Simulation simulation)
        {
            double[][] channelRate = Enumerable
                .Range(0, simulation.UserCount)
                .Select(x => new double[simulation.Duration])
                .ToArray();

            return channelRate;
        }

    }
}
