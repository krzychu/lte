﻿using Lte.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paper.Plots
{
    abstract class PlotBase : IPlot
    {
        private string _title;
        protected IEnumerable<ForSimulation> Simulations;

        public PlotBase(string title, params ForSimulation[] simulations)
        {
            _title = title;
            Simulations = simulations;
        }

        public abstract void Draw(IPlotBuilder plotter, Lte.Core.IStorage storage);

        public override string ToString()
        {
            return _title;
        }
    }
}
