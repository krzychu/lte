﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Model
{
    public class Simulation
    {
        public int Id { get; set; }

        public int UserCount { get; set; }
        public int Duration { get; set; }
        public int RepetitionCount { get; set; }

        public string SchedulerJson { get; set; }
        public string ChannelJson { get; set; }

        public virtual ICollection<Execution> Executions { get; set; }

        public Simulation()
        {
            Executions = new List<Execution>();
        }
    }
}
