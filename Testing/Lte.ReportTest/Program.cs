﻿using Lte.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lte.ReportTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //ReportApp.Run(ReportApp.GetPlotsInAssembly(Assembly.GetExecutingAssembly()), null, "Test");
            PythonTest.TestPlot();
        }
    }
}
