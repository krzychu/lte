﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lte.Sketchpad
{
    public partial class SketchpadWindow : Form
    {
        public SketchpadWindow()
        {
            InitializeComponent();
        }

        private void SketchpadWindow_Load(object sender, EventArgs e)
        {
            var model = new PlotModel();
            PlotSketchpad.Draw(model);
            plotView.Model = model;
        }
    }
}
