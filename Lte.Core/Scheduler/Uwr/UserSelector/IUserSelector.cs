﻿using Lte.Core.Scheduler.Uwr.StateUpdater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lte.Core.Scheduler.Uwr.UserSelector
{
    public interface IUserSelector
    {
        int SelectActiveUser(Constants constants, State currentState, IStateUpdater stateUpdater, double[] channelRates);
    }
}
