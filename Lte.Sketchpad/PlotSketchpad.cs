﻿using Lte.Core;
using Lte.Core.Channel.EncodedRayleigh;
using Lte.Core.Channel.SimpleRayleigh;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Sketchpad
{
    public static class PlotSketchpad
    {
        public static void Draw(PlotModel model)
        {
            var users = new []
            {
                new 
                {
                    Mean = EncodedRayleighChannel.UserClasses.PoorSnrDb,
                    Title = "Poor",
                    Color = OxyColors.Red
                },

                new 
                {
                    Mean = EncodedRayleighChannel.UserClasses.AverageSnrDb,
                    Title = "Average",
                    Color = OxyColors.Orange
                },

                new 
                {
                    Mean = EncodedRayleighChannel.UserClasses.GoodSnrDb,
                    Title = "Good",
                    Color = OxyColors.Green
                },
            };


            var series = users.Select(x => new LineSeries() 
            {
                Color = x.Color,
                Title = x.Title
            }).ToArray();

            var settings = SimpleRayleighSettings.FromUserClasses(
                20e6,
                1e-3,
                35,
                1, 1, 1);

            var channel = settings.GetChannel(new SimulationContext());

            foreach (var t in Enumerable.Range(0, 200))
            {
                var rates = channel.GetChannelRates();
                foreach(var i in Enumerable.Range(0, users.Length))
                    series[i].Points.Add(new DataPoint(t, rates[i]));
            }

            foreach(var s in series)
                model.Series.Add(s);
        }
    }
}
