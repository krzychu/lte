﻿using Lte.Core.Scheduler.Uwr.StateUpdater;
using Lte.Core.Scheduler.Uwr.UserSelector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr
{
    public class UwrScheduler : IScheduler
    {
        private Constants _constants;
        private State _currentState;
        private IStateUpdater _stateUpdater;
        private IUserSelector _userSelector;

        public UwrScheduler(SimulationContext cxt, double maxChannelCapacity, IStateUpdater updater, IUserSelector userSelector)
        {
            _constants = new Constants(cxt.UserCount, maxChannelCapacity);
            _currentState = new State(cxt.UserCount);
            _stateUpdater = updater;
            _userSelector = userSelector;
        }

        public int SelectActiveUser(double[] channelRates)
        {
            //Debug.Assert(Potential.IsFeasible(_constants, _currentState));

            int selected = _userSelector.SelectActiveUser(_constants, _currentState, _stateUpdater, channelRates);
            _currentState = _stateUpdater.GetNextState(_constants, _currentState, channelRates, selected);
            return selected;
        }
    }
}
