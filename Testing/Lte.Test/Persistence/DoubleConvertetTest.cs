﻿using Lte.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Persistence
{
    [TestClass]
    public class DoubleConvertetTest
    {
        [TestMethod]
        public void ConvertsFromAndToBinary()
        {
            double[] target = new[] 
            {
                1.0, 345.0, 567.0, 23e20
            };

            var bytes = DoubleConverter.ToBytes(target);
            Assert.IsTrue(Enumerable.SequenceEqual(target, DoubleConverter.ToDouble(bytes)));
        }
    }
}
