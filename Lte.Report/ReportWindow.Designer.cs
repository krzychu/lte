﻿namespace Lte.Report
{
    partial class ReportWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportWindow));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.plotComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.plotView = new OxyPlot.WindowsForms.PlotView();
            this.dumpAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plotComboBox,
            this.dumpAllButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1057, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip";
            // 
            // plotComboBox
            // 
            this.plotComboBox.Name = "plotComboBox";
            this.plotComboBox.Size = new System.Drawing.Size(400, 25);
            this.plotComboBox.SelectedIndexChanged += new System.EventHandler(this.plotComboBox_SelectedIndexChanged);
            // 
            // plotView
            // 
            this.plotView.BackColor = System.Drawing.Color.White;
            this.plotView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotView.Location = new System.Drawing.Point(0, 25);
            this.plotView.Name = "plotView";
            this.plotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plotView.Size = new System.Drawing.Size(1057, 638);
            this.plotView.TabIndex = 1;
            this.plotView.Text = "plotView";
            this.plotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // dumpAllButton
            // 
            this.dumpAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dumpAllButton.Image = ((System.Drawing.Image)(resources.GetObject("dumpAllButton.Image")));
            this.dumpAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dumpAllButton.Name = "dumpAllButton";
            this.dumpAllButton.Size = new System.Drawing.Size(119, 22);
            this.dumpAllButton.Text = "Dump All To Python";
            this.dumpAllButton.Click += new System.EventHandler(this.dumpAllButton_Click);
            // 
            // ReportWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 663);
            this.Controls.Add(this.plotView);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ReportWindow";
            this.ShowIcon = false;
            this.Text = "Report";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox plotComboBox;
        private OxyPlot.WindowsForms.PlotView plotView;
        private System.Windows.Forms.ToolStripButton dumpAllButton;
    }
}