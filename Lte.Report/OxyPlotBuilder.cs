﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Report
{
    public class OxyPlotBuilder : IPlotBuilder
    {
        public string XLabel { get; set; }
        public string YLabel { get; set; }

        private PlotModel _model = new PlotModel();

        public PlotModel GetModel()
        {
            _model.Axes.Add(new LinearAxis(AxisPosition.Bottom, XLabel));
            _model.Axes.Add(new LinearAxis(AxisPosition.Left, YLabel));
            _model.LegendPosition = LegendPosition.BottomRight;

            return _model;
        }

        public void Plot(double[] xs, double[] ys, PlotStyle style, Color color, string title)
        {
            if(xs == null)
                throw new ArgumentNullException("xs");

            if(ys == null)
                throw new ArgumentNullException("ys");

            if(xs.Length != ys.Length)
                throw new ArgumentException("xs and ys have to be of the same length");

            Series series = (style == PlotStyle.Line) ? DrawLine(xs, ys, color) : DrawScatter(xs, ys, color);
            series.Title = title;

            _model.Series.Add(series);
        }

        private Series DrawScatter(double[] xs, double[] ys, Color color)
        {
            var series = new ScatterSeries();
            series.MarkerFill = ConvertColor(color);
            series.Points.AddRange(Enumerable.Zip(xs, ys, (x, y) => new ScatterPoint(x, y, 1.0)));
            return series;
        }

        private Series DrawLine(double[] xs, double[] ys, Color color)
        {
            var series = new LineSeries();
            series.Points.AddRange(Enumerable.Zip(xs, ys, (x, y) => new DataPoint(x, y)));
            series.Color = ConvertColor(color);
            return series;
        }

        private OxyColor ConvertColor(Color color)
        {
            return OxyColor.FromRgb(color.R, color.G, color.B);
        }


        public void Plot(double[] ys, PlotStyle style, Color color, string title)
        {
            var xs = Enumerable.Range(0, ys.Length).Select(x => (double)x).ToArray();
            Plot(xs, ys, style, color, title);
        }
    }
}
