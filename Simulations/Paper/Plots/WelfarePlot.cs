﻿using Lte.Core;
using Lte.Core.Postprocessing;
using Lte.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paper.Plots
{
    class WelfarePlot : PlotBase
    {
        public WelfarePlot(string title, params ForSimulation[] simulations)
            : base(title, simulations)
        {}

        public override void Draw(IPlotBuilder plotter, IStorage storage)
        {
            plotter.XLabel = "Time [ms]";
            plotter.YLabel = "Welfare";

            foreach (var simulationInfo in Simulations)
            {
                var simulation = storage.Simulations
                    .Where(x => x.Id == simulationInfo.SimulationId)
                    .First();

                var avg = new double[simulation.Duration];

                foreach (var execution in simulation.Executions)
                {
                    var kpis = storage.GetKpiValues(execution.Id, Welfare.WelfareKpiName, -1);
                    for (int i = 0; i < kpis.Length; i++)
                        avg[i] += kpis[i] / simulation.UserCount;
                }

                plotter.Plot(
                    avg,
                    PlotStyle.Line,
                    simulationInfo.Color,
                    simulationInfo.Label
                );
            }
        }
    }
}
