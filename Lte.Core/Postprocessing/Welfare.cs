﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public class Welfare : IPostprocessor
    {
        public const string WelfareKpiName = "Welfare"; 

        public string KpiName
        {
            get { return WelfareKpiName; }
        }

        public void Run(IStorage storage, Simulation simulation)
        {
            int n = simulation.UserCount;

            foreach (var execution in simulation.Executions)
            { 
                double[] welfare = new double[simulation.Duration];

                foreach (var user in Enumerable.Range(0, simulation.UserCount))
                {
                    var otherKpi = storage.GetKpiValues(execution.Id, SumOfTransmissions.SumOfTransmissionsKpiName, user);
                    foreach (var t in Enumerable.Range(0, simulation.Duration))
                        welfare[t] += Math.Log(otherKpi[t]);
                }

                storage.SetKpiValues(execution.Id, WelfareKpiName, -1, 
                    welfare.Select(x => Math.Max(0, x)).ToArray());
            }
        }
    }
}
