﻿using Lte.Core;
using Lte.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Paper.Plots
{
    public class ChannelRatePlot : IPlot
    {
        private const int Duration = 100;
        private int _simulationId;
        private string _title;

        public ChannelRatePlot(string title, int simulationId)
        {
            _simulationId = simulationId;
            _title = title;
        }

        public void Draw(IPlotBuilder plotter, IStorage storage)
        {
            var simulation = storage.Simulations.Where(x => x.Id == _simulationId).First();
            int executionId = simulation.Executions.First().Id;

            var poor = storage.GetKpiValues(executionId, SimulationRunner.ChannelRateKpi, 0).Take(Duration).ToArray();
            var average = storage.GetKpiValues(executionId, SimulationRunner.ChannelRateKpi, 1).Take(Duration).ToArray();
            var good = storage.GetKpiValues(executionId, SimulationRunner.ChannelRateKpi, 2).Take(Duration).ToArray();

            plotter.XLabel = "Time [ms]";
            plotter.YLabel = "Bits per Scheduling Interval";
            plotter.Plot(poor, PlotStyle.Line, Color.Blue, "Poor");
            plotter.Plot(average, PlotStyle.Line, Color.Green, "Average");
            plotter.Plot(good, PlotStyle.Line, Color.Red, "Good");
        }

        public override string ToString()
        {
            return _title;
        }
    }
}
