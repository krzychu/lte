﻿using Lte.Core;
using Lte.Core.Scheduler;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Core.Scheduler
{
    [TestClass]
    public class UtilTest
    {
        [TestMethod]
        public void ArgMaxWorks()
        {
            var args = new[] { 1.0, 2, 3, 4, 3, 2, 1};

            Assert.AreEqual(3, Util.ArgMax(args));
        }
    }
}
