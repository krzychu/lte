﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.ProportionalFair
{
    public class ProportionalFairScheduler : IScheduler
    {
        private double[] _average;
        private double _tau;

        public ProportionalFairScheduler(SimulationContext context, double tau)
        {
            var n = context.UserCount;
            _average = new double[n];
            _tau = tau;
        }

        public int SelectActiveUser(double[] channelRates)
        {
            Debug.Assert(channelRates.Length == _average.Length);
            var n = _average.Length;

            var relative = Enumerable
                .Zip(channelRates, _average, (x, y) => x / y)
                .ToArray();

            int best = 0;
            for (int i = 0; i < n; i++)
                best = (relative[i] > relative[best]) ? i : best;


            for (int i = 0; i < n; i++)
            {
                _average[i] *= 1 - _tau;

                if (i == best)
                    _average[i] += _tau * channelRates[i];
            }

            return best;
        }
    }
}
