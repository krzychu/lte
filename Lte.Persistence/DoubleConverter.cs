﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Persistence
{
    public static class DoubleConverter
    {
        public static byte[] ToBytes(double[] values)
        {
            return values.SelectMany(x => BitConverter.GetBytes(x)).ToArray();
        }

        public static double[] ToDouble(byte[] bytes)
        {
            return Enumerable.Range(0, bytes.Length / sizeof(double))
                .Select(x => BitConverter.ToDouble(bytes, sizeof(double) * x))
                .ToArray();
        }
    }
}
