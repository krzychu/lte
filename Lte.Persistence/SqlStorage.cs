﻿using Lte.Core;
using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lte.Persistence
{
    public class SqlStorage : IStorage
    {
        private LteDbContext _efContext;

        public IQueryable<Simulation> Simulations
        {
            get { return _efContext.Simulations; }
        }

        public SqlStorage(string connectionString, bool dropDb = false)
        {
            if (dropDb)
                Database.SetInitializer(new SqlStorageInitializer(new DropCreateDatabaseAlways<LteDbContext>()));
            else
                Database.SetInitializer(new SqlStorageInitializer(new NullDatabaseInitializer<LteDbContext>()));

            _efContext = new LteDbContext(connectionString);
        }

        public void AddSimulation(Simulation simulation)
        {
            _efContext.Simulations.Add(simulation);
        }

        public void Flush()
        {
            _efContext.SaveChanges();
        }

        public double[] GetKpiValues(int executionId, string kpiName, int userId)
        {
            using (var connection = new SqlConnection(_efContext.Database.Connection.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = @"
                    SELECT
                        [Values]
                    FROM
                        [KpiValues]
                    WHERE
                        [Execution_id] = @execution 
                        AND [KpiName] = @kpiName
                        AND [User_Id] = @user
                    ";

                command.Parameters.Add("@execution", SqlDbType.Int);
                command.Parameters.Add("@kpiName", SqlDbType.VarChar);
                command.Parameters.Add("@user", SqlDbType.Int);

                command.Parameters["@execution"].Value = executionId;
                command.Parameters["@kpiNAme"].Value = kpiName;
                command.Parameters["@user"].Value = userId;

                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read())
                        return null;

                    return DoubleConverter.ToDouble((byte[])reader[0]);
                }
            }
        }

        public void SetKpiValues(int executionId, string kpiName, int userId, double[] valuesOverTime)
        {
            using (var connection = new SqlConnection(_efContext.Database.Connection.ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = @"
                    INSERT INTO
                        [KpiValues]
                    VALUES
                        (@execution, @user, @kpiName, @values) 
                    ";

                command.Parameters.Add("@execution", SqlDbType.Int);
                command.Parameters.Add("@kpiName", SqlDbType.VarChar);
                command.Parameters.Add("@user", SqlDbType.Int);
                command.Parameters.Add("@values", SqlDbType.VarBinary);

                command.Parameters["@execution"].Value = executionId;
                command.Parameters["@kpiName"].Value = kpiName;
                command.Parameters["@user"].Value = userId;
                command.Parameters["@values"].Value = DoubleConverter.ToBytes(valuesOverTime);
                command.ExecuteNonQuery();
            }
        }

        public void Dispose()
        {
            _efContext.Dispose();
        }
    }
}
