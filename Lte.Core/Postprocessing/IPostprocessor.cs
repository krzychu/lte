﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public interface IPostprocessor
    {
        string KpiName { get; }

        void Run(IStorage storage, Simulation simulation);
    }
}
