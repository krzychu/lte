﻿using Lte.Core.Scheduler.Uwr.StateUpdater;
using Lte.Core.Scheduler.Uwr.UserSelector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr
{
    public class UwrSchedulerSettings : ISchedulerSettings
    {
        public IUserSelector UserSelector { get; set; }
        public IStateUpdater StateUpdater { get; set; }

        public string Name
        {
            get { return String.Format("UWR ({0}, {1})", UserSelector.GetType().Name, StateUpdater.GetType().Name); }
        }

        public double MaxChannelCapacity { get; set; }

        public IScheduler GetScheduler(SimulationContext context)
        {
            return new UwrScheduler(context, MaxChannelCapacity, StateUpdater, UserSelector);
        }
    }
}
