﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Persistence
{
    public class LteDbContext : DbContext
    {
        public virtual DbSet<Simulation> Simulations { get; set; }

        public LteDbContext(string connectionString)
            : base(connectionString)
        {
        }
    }
}
