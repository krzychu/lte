﻿using Lte.Core;
using Lte.Core.Postprocessing;
using Lte.Report;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paper.Plots
{
    class TotalVsPossible : PlotBase
    {
        private double _slope;
        private double _shift;

        public TotalVsPossible(string title, double slope, double shift, params ForSimulation[] simulations)
            : base(title, simulations)
        {
            _slope = slope;
            _shift = shift;
        }

        public override void Draw(IPlotBuilder plotter, IStorage storage)
        {
            double max = 0;

            plotter.XLabel = "Possible [bits]";
            plotter.YLabel = "Total [bits]";

            foreach (var simulationInfo in Simulations)
            {
                var simulation = storage.Simulations
                    .Where(x => x.Id == simulationInfo.SimulationId)
                    .First();

                var globalTotal = new List<double>();
                var globalPossible = new List<double>();

                foreach (var user in Enumerable.Range(0, simulation.UserCount))
                {
                    double[] meanTotalForUser = new double[simulation.Duration];
                    double[] meanPossibleForUser = new double[simulation.Duration];
                    foreach (var execution in simulation.Executions)
                    {
                        var total = storage.GetKpiValues(execution.Id, SumOfTransmissions.SumOfTransmissionsKpiName, user);
                        var possible = storage.GetKpiValues(execution.Id, SumOfChannelRate.SumOfChannelRateKpiName, user);

                        foreach (var time in Enumerable.Range(0, simulation.Duration))
                        {
                            meanTotalForUser[time] += 1.0 / simulation.RepetitionCount * total[time];
                            meanPossibleForUser[time] += 1.0 / simulation.RepetitionCount * possible[time];
                        }
                    }
                    globalTotal.AddRange(meanTotalForUser);
                    globalPossible.AddRange(meanPossibleForUser);
                }

                plotter.Plot(
                    globalPossible.ToArray(),
                    globalTotal.ToArray(),
                    PlotStyle.Scatter,
                    simulationInfo.Color,
                    simulationInfo.Label
                );

                max = Math.Max(max, globalPossible.Max());
            }

            var xs = new[] { 0, max };

            plotter.Plot(
                   xs,
                   xs.Select(x => x * _slope + _shift).ToArray(),
                   PlotStyle.Line,
                   Color.Black,
                   String.Format("{0}x + {1}", _slope.ToString(CultureInfo.InvariantCulture), _shift.ToString(CultureInfo.InvariantCulture))
               );
        }
    }
}
