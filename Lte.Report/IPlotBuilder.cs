﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Report
{
    public enum PlotStyle
    {
        Scatter, 
        Line
    }

    public interface IPlotBuilder
    {
        void Plot(double[] xs, double[] ys, PlotStyle style, Color color, string title);
        void Plot(double[] ys, PlotStyle style, Color color, string title);
        
        string XLabel { get; set; }
        string YLabel { get; set; }
    }
}
