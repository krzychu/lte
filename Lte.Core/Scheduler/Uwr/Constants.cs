﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr
{
    public class Constants
    {
        public IEnumerable<int> Users { get { return Enumerable.Range(0, UserCount); } }
        public int UserCount { get; private set; }
        public double A { get; private set; }
        public double Delta { get; private set; }
        public double MaxChannelCapacity { get; private set; }

        public Constants(int userCount, double maxChannelCapacity)
        {
            UserCount = userCount;
            Delta = 1.0 / (1 + UserCount);
            MaxChannelCapacity = maxChannelCapacity;
            A = 12 * userCount * userCount * MaxChannelCapacity;
        }
    }
}
