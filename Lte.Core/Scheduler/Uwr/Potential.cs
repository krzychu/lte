﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr
{
    public static class Potential
    {
        public static double GetValue(double[] xs)
        { 
            int n = xs.Length;

            double value = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    value += 1.0 / (xs[i] * xs[j]);
                }
            }

            return value;
        }

        public static double[] GetDerivatives(double[] xs)
        {
            int n = xs.Length;
            double[] derivatives = new double[n];

            for (int i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                { 
                    if(j != i)
                        derivatives[i] += 1.0 / xs[j];
                }

                derivatives[i] *= -1.0 / (xs[i] * xs[i]);
            }

            return derivatives;
        }

        public static double[] GetPosition(Constants constants, State state)
        {
            return constants.Users
                .Select(user => constants.A + state.Total[user] - constants.Delta * state.Possible[user])
                .ToArray();
        }

        public static bool IsFeasible(Constants constants, double[] position)
        {
            if (position.Any(x => x < 0))
                return false;

            int n = constants.UserCount;
            double maxPotential = (n * (n - 1.0)) / 2.0 / constants.A / constants.A;

            return GetValue(position) <= maxPotential;
        }
    }
}
