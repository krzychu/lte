﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public class PostprocessingRunner
    {
        private List<IPostprocessor> _stages = new List<IPostprocessor>();

        public void AddStage(IPostprocessor stage)
        {
            _stages.Add(stage);
        }

        public void Run(IStorage storage)
        {
            foreach (var simulation in storage.Simulations.ToArray())
            {
                foreach (var stage in _stages)
                {
                    stage.Run(storage, simulation);
                }
            }
            storage.Flush();
        }
    }
}
