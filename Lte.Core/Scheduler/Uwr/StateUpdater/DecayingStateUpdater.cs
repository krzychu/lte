﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr.StateUpdater
{
    public class DecayingStateUpdater : IStateUpdater
    {
        public double DecayRate { get; set; }

        public DecayingStateUpdater()
        {
            DecayRate = 1.0;
        }

        public State GetNextState(Constants constants, State current, double[] channelRates, int selected)
        {
            var newPossible = constants.Users
                .Select(user => current.Possible[user] * DecayRate + channelRates[user])
                .ToArray();

            var newTotal = constants.Users
                .Select(user => current.Total[user] * DecayRate + ((user == selected) ? channelRates[user] : 0))
                .ToArray();

            return new State(newTotal, newPossible);
        }
    }
}
