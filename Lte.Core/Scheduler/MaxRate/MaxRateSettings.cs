﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.MaxRate
{
    public class MaxRateSettings : ISchedulerSettings
    {
        public string Name
        {
            get { return "Max Rate"; }
        }

        public IScheduler GetScheduler(SimulationContext context)
        {
            return new MaxRateScheduler(context.UserCount);
        }
    }
}
