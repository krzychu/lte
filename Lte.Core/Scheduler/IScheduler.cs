﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler
{
    public interface IScheduler
    {
        int SelectActiveUser(double[] channelRates);
    }
}
