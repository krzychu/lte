﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lte.Core.Scheduler.ProportionalFair;
using Lte.Core;

namespace Lte.Test.Core.Scheduler.ProportionalFair
{
    [TestClass]
    public class ProportionalFairSchedulerTest
    {
        [TestMethod]
        public void ReturnsUserWithBestRatio()
        {
            var context = new SimulationContext()
            {
                Seed = 3,
                UserCount = 3,
            };

            var target = new ProportionalFairScheduler(context, 0.7);

            Assert.AreEqual(0, target.SelectActiveUser(new[] { 1.0, 0.0, 0.0 }));
            Assert.AreEqual(1, target.SelectActiveUser(new[] { 0.0, 1.0, 0.0 }));
            Assert.AreEqual(2, target.SelectActiveUser(new[] { 0.0, 0.0, 1.0 }));
        
            // memorized rates are 0.063 0.21 0.7

            Assert.AreEqual(0, target.SelectActiveUser(new[] { 0.064, 0.21, 0.7 }));            
        }
    }
}
