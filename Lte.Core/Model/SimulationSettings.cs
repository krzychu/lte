﻿using Lte.Core.Channel;
using Lte.Core.Scheduler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Model
{
    public class SimulationSettings
    {
        public int UserCount { get; set; }
        public int Duration { get; set; }
        public int RepetitionCount { get; set; }
        public ISchedulerSettings SchedulerSettings { get; set; }
        public IChannelSettings ChannelSettings { get; set; }

        public Simulation GetSimulation()
        {
            var sim = new Simulation();
            sim.UserCount = UserCount;
            sim.Duration = Duration;
            sim.RepetitionCount = RepetitionCount;
            sim.SchedulerJson = JsonConvert.SerializeObject(SchedulerSettings, Formatting.Indented);
            sim.ChannelJson = JsonConvert.SerializeObject(ChannelSettings, Formatting.Indented);
            return sim;
        }
    }
}
