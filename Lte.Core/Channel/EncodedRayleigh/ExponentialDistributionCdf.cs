﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.EncodedRayleigh
{
    public class ExponentialDistributionCdf
    {
        private double _linearMean;

        public ExponentialDistributionCdf(double linearMean)
        {
            _linearMean = linearMean;   
        }

        public double GetValue(double z)
        {
            return 1.0 - Math.Exp(- z / _linearMean);
        }

        public double GetInverse(double y)
        {
            return - _linearMean * Math.Log(1 - y);
        }
    }
}
