﻿using Lte.Core.Model;
using Lte.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Persistence
{
    [TestClass]
    public class StorageTest
    {
        const string dbConnectionString = @"Server=(local)\SQLEXPRESS; Initial Catalog=unittest; Integrated Security=True";

        private SqlStorage sqlStorage;

        [TestInitialize]
        public void Initialize()
        {
            sqlStorage = new SqlStorage(dbConnectionString, true);
        }

        [TestCleanup]
        public void CleanUp()
        {
            sqlStorage.Dispose();
        }

        [TestMethod]
        public void CreationWorks()
        {
            Assert.IsNotNull(sqlStorage);
        }

        [TestMethod]
        public void KpiValuesCanBePersisted()
        {
            var simulation = new Simulation()
            {
                ChannelJson = "",
                SchedulerJson = ""
            };

            sqlStorage.AddSimulation(simulation);
            Dictionary<int, double[]> expected = new Dictionary<int, double[]>();

            foreach (var i in Enumerable.Range(0, 10))
            {
                var execution = new Execution();
                simulation.Executions.Add(execution);
                sqlStorage.Flush();

                var values = Enumerable.Range(100 * i, 100 * (i + 1)).Select(x => (double)x).ToArray();
                sqlStorage.SetKpiValues(execution.Id, "testKpi", -1, values);

                expected[execution.Id] = values;
            }

            foreach (var executionId in expected.Keys)
            {
                var values = sqlStorage.GetKpiValues(executionId, "testKpi", -1);
                Assert.AreEqual(expected[executionId].Length, values.Length);
                Assert.IsTrue(Enumerable.SequenceEqual(expected[executionId], values));
            }
        }
    }
}
