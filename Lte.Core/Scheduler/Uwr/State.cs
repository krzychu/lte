﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr
{
    public class State
    {
        public double[] Total { get; private set; }
        public double[] Possible { get; private set; }

        public State(int userCount)
            : this(new double[userCount], new double[userCount])
        {
        }

        public State(double[] total, double[] possible)
        {
            if (total.Length != possible.Length)
                throw new ArgumentException("total length should match possible length");

            Total = total;
            Possible = possible;
        }
    }
}
