﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Model
{
    public class Execution
    {
        public int Id { get; set; }
        public int Seed { get; set; }
    }
}
