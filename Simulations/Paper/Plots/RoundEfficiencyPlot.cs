﻿using Lte.Core;
using Lte.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paper.Plots
{
    class RoundEfficiencyPlot : PlotBase
    {
        public RoundEfficiencyPlot(string title, params ForSimulation[] simulations)
            : base(title, simulations)
        {
        }

        public override void Draw(IPlotBuilder plotter, IStorage storage)
        {
            plotter.XLabel = "Efficiency";
            plotter.YLabel = "Probability";

            foreach (var simulationInfo in Simulations)
            { 
                var simulation = storage.Simulations
                    .Where(x => x.Id == simulationInfo.SimulationId)
                    .First();

                var efficiency = new List<double>();
                foreach (var execution in simulation.Executions)
                {
                    var possible = new double[simulation.Duration];
                    var total = new double[simulation.Duration];

                    foreach (var user in Enumerable.Range(0, simulation.UserCount))
                    {
                        var channelRates = storage.GetKpiValues(execution.Id, SimulationRunner.ChannelRateKpi, user);
                        var transmitted = storage.GetKpiValues(execution.Id, SimulationRunner.TransmittedKpi, user);

                        foreach(var time in Enumerable.Range(0, simulation.Duration))
                        {
                            possible[time] += channelRates[time];
                            total[time] += transmitted[time];
                        }
                    }

                    efficiency.AddRange(Enumerable.Zip(total, possible, (x, y) => x / y).Where(x => !double.IsInfinity(x)));
                }

                efficiency.Sort();
                double[] counts = Enumerable.Range(0, efficiency.Count)
                    .Select(x => x / ((double)efficiency.Count))
                    .ToArray();

                plotter.Plot(efficiency.ToArray(), counts, PlotStyle.Line, simulationInfo.Color, simulationInfo.Label);
            }
        }
    }
}
