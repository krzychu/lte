﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core
{
    public class ProgressCounter
    {
        private double _target;
        private double _count;

        public int ProgressPercentage 
        {
            get 
            {
                return (int)Math.Round(100.0 * _count / _target);
            } 
        }

        public ProgressCounter(int target)
        {
            Trace.Assert(target != 0);
            _target = target;
            _count = 0;
        }

        public void Next()
        {
            _count++;
        }
    }
}
