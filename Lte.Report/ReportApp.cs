﻿using Lte.Core;
using Lte.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lte.Report
{
    public static class ReportApp
    {
        [STAThread]
        public static void Run(IEnumerable<IPlot> plots, IStorage storage, string title)
        {
            Application.EnableVisualStyles();
            Application.Run(new ReportWindow(plots, storage, title));
        }

        public static IEnumerable<IPlot> GetPlotsInAssembly(Assembly assembly)
        {
            var plots = assembly
                .GetTypes()
                .Where(x => typeof(IPlot).IsAssignableFrom(x))
                .Where(x => !x.IsAbstract)
                .Select(x => Activator.CreateInstance(x))
                .Cast<IPlot>()
                .ToArray();

            return plots;
        }
    }
}
