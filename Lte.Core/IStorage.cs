﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core
{
    public interface IStorage : IDisposable
    {
        IQueryable<Simulation> Simulations { get; }

        void AddSimulation(Simulation simulation);

        void SetKpiValues(int executionId, string kpiName, int userId, double[] valuesOverTime);

        double[] GetKpiValues(int executionId, string kpiName, int userId);

        void Flush();
    }
}
