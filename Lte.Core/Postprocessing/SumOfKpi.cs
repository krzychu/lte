﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public class SumOfKpi : IPostprocessor
    {
        private string _summedKpi;

        public string KpiName { get; private set; }

        public SumOfKpi(string summedKpi, string kpiName)
        {
            KpiName = kpiName;
            _summedKpi = summedKpi;
        }

        public void Run(IStorage storage, Simulation simulation)
        {
            foreach (var user in Enumerable.Range(0, simulation.UserCount))
            {
                foreach (var execution in simulation.Executions)
                {
                    var summedValues = storage.GetKpiValues(execution.Id, _summedKpi, user);
                    double sum = 0;
                    double[] result = new double[simulation.Duration];
                    foreach (var time in Enumerable.Range(0, summedValues.Length))
                    {
                        sum += summedValues[time];
                        result[time] = sum;
                    }

                    storage.SetKpiValues(execution.Id, KpiName, user, result);
                }
            }

            storage.Flush();
        }   
    }
}
