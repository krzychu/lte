﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.RoundRobin
{
    public class RoundRobinScheduler : IScheduler
    {
        private SimulationContext _context;
        
        public RoundRobinScheduler(SimulationContext context)
        {
            _context = context;
        }

        public int SelectActiveUser(double[] channelRates)
        {
            return _context.Time % _context.UserCount;
        }
    }
}
