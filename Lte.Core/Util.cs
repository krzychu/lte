﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core
{
    public static class Util
    {
        private static double _log2Scale = Math.Log(2);

        public static int ArgMax(this IEnumerable<double> xs)
        {
            int best = -1;
            double bestValue = double.MinValue;
            int i = 0;
            foreach (var x in xs)
            {
                if (x > bestValue)
                {
                    best = i;
                    bestValue = x;
                }
                i++;
            }

            if (best == -1)
                throw new InvalidOperationException("Can't find argmax of empty enumerable");

            return best;
        }

        public static int ArgMin(this IEnumerable<double> xs)
        {
            return xs.Select(x => -x).ArgMax();
        }

        public static double ToDb(double linear)
        {
            return 10 * Math.Log10(linear);
        }

        public static double ToLinear(double db)
        {
            return Math.Pow(10, db / 10);
        }

        public static double Log2(double v)
        {
            return Math.Log(v) / _log2Scale;
        }
    }
}
