﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public class SumOfChannelRate : SumOfKpi
    {
        public const string SumOfChannelRateKpiName = "SumOfChannelRate";

        public SumOfChannelRate()
            : base(SimulationRunner.ChannelRateKpi, SumOfChannelRateKpiName)
        {
        }
    }
}
