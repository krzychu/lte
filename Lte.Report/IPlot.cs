﻿using Lte.Core;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Report
{
    public interface IPlot
    {
        void Draw(IPlotBuilder plotter, IStorage storage);
    }
}
