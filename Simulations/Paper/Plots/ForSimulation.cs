﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Paper.Plots
{
    public struct ForSimulation
    {
        public int SimulationId { get; set; }
        public string Label { get; set; }
        public Color Color { get; set; }
    }
}
