﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Postprocessing
{
    public class SumOfTransmissions : SumOfKpi
    {
        public const string SumOfTransmissionsKpiName = "SumOfTransmissions";

        public SumOfTransmissions()
            : base(SimulationRunner.TransmittedKpi, SumOfTransmissionsKpiName)
        {
        }
    }
}
