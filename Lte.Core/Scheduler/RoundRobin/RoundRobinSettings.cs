﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.RoundRobin
{
    public class RoundRobinSettings : ISchedulerSettings
    {
        public string Name
        {
            get { return "RoundRobin"; }
        }

        public IScheduler GetScheduler(SimulationContext context)
        {
            return new RoundRobinScheduler(context);
        }
    }
}
