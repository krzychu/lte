﻿using Lte.Core.Scheduler.Uwr.StateUpdater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr.UserSelector
{
    public class ClassicUserSelector : IUserSelector
    {
        public int SelectActiveUser(Constants constants, State currentState, IStateUpdater stateUpdater, double[] channelRates)
        {
            if (constants.UserCount != channelRates.Length)
                new ArgumentException("UserCount does not match channel rates length");

            var currentPosition = Potential.GetPosition(constants, currentState);
            var thr = 6 * constants.UserCount * constants.UserCount;
            var obligatoryShift = constants.Users
                .Select(user => -constants.Delta * channelRates[user])
                .ToArray();

            bool smallChange = constants
                .Users
                .All(i => Math.Abs(obligatoryShift[i]) <= currentPosition[i] / thr);

            var selected = 0;
            if (smallChange)
                selected = Enumerable.Zip(
                    obligatoryShift,
                    Potential.GetDerivatives(currentPosition),
                    (v, d) => v * d).ArgMax();
            else
                selected = obligatoryShift.ArgMin();
            
            return selected;   
        }
    }
}
