﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.EncodedRayleigh
{
    public static class BitsPerSymbol
    {
        private static double[] _thresholds = 
        {
            0.54, 
            0.91,
            1.2,
            1.41,
            2.81,
            4.78,
            6.3,
            7.41,
            12.3,
            26.91,
            33.13,
            38.01,
            85.11,
            141.25,
            181.97
        };

        private static double[] _bps =
        {
            0.25,
            0.4,
            0.5,
            0.67,
            1,
            1.3,
            1.5,
            1.6,
            2,
            2.66,
            3,
            3.2,
            4,
            4.5,
            4.8
        };

        public static double GetBitsPerSymbol(double linearSnr)
        {
            if (linearSnr < _thresholds[0])
                return 0;

            int idx = Enumerable.Range(0, _bps.Length)
                .Last(x => _thresholds[x] < linearSnr);

            return _bps[idx];
        }
    }
}
