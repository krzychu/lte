﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Persistence
{
    public class SqlStorageInitializer : IDatabaseInitializer<LteDbContext>
    {
        private IDatabaseInitializer<LteDbContext> _next;

        private const string _valueCacheSql = @"
            IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'KpiValues')
            CREATE TABLE [dbo].[KpiValues](
	            [Execution_Id] [int] NOT NULL,
	            [User_Id] [int] NOT NULL,
	            [KpiName] [char](256) NOT NULL,
                [Values] [varbinary](max) NOT NULL

                CONSTRAINT PK_KpiValues PRIMARY KEY ([Execution_Id], [User_Id], [KpiName])
            )";

        public SqlStorageInitializer()
            : this(new CreateDatabaseIfNotExists<LteDbContext>())
        {

        }

        public SqlStorageInitializer(IDatabaseInitializer<LteDbContext> next)
        {
            _next = next;
        }

        public void InitializeDatabase(LteDbContext context)
        {
            _next.InitializeDatabase(context);
            context.Database.ExecuteSqlCommand(_valueCacheSql);
        }
    }
}
