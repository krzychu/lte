﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel
{
    public interface IChannel
    {
        double[] GetChannelRates();
    }
}
