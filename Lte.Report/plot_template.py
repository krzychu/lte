﻿import matplotlib.pyplot as plt

data = {}

with open('__data_path__') as source:
    for line in source.readlines():
        parts = line.split(',')
        series = parts[0].strip()
        axis = parts[1].strip()
        values = map(float, parts[2:])
         
        if series not in data:
            data[series] = {}

        data[series][axis] = values    

__plot__

plt.xlabel('__xlabel__')
plt.ylabel('__ylabel__')
plt.legend()
plt.show()

