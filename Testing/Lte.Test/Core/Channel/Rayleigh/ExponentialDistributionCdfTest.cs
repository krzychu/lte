﻿using Lte.Core.Channel.EncodedRayleigh;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Core.Channel.Rayleigh
{
    [TestClass]
    public class ExponentialDistributionCdfTest
    {
        [TestMethod]
        public void InverseWorks()
        {
            var cdf = new ExponentialDistributionCdf(2);

            for (double x = 0; x < 10; x += 0.1)
            {
                Assert.AreEqual(x, cdf.GetInverse(cdf.GetValue(x)), 1e-6);
            }
        }
    }
}
