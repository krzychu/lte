﻿using Lte.Report;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.ReportTest
{
    public class ScatterTest : IPlot
    {
        public void Draw(IPlotBuilder plotter, Core.IStorage storage)
        {
            var xs = Enumerable.Range(0, 100).Select(x => (double)x).ToArray();
            plotter.Plot(xs, xs, PlotStyle.Scatter, Color.Blue, "Random dots");
        }
    }
}
