﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.EncodedRayleigh
{
    public class EncodedRayleighChannel : IChannel
    {
        public static class UserClasses
        {
            public const double PoorSnrDb = 7;
            public const double AverageSnrDb = 16;
            public const double GoodSnrDb = 23;
        }

        private ExponentialDistributionCdf[] _cdfs;
        private Random _gen;
        private double _symbolsPerSchedulingInterval;

        public EncodedRayleighChannel(double symbolsPerSchedulingInverval, double[] dbMean, int seed)
        {
            SetMean(dbMean);
            _gen = new Random(seed);
            _symbolsPerSchedulingInterval = symbolsPerSchedulingInverval;
        }

        internal void SetMean(double[] dbMean)
        {
            _cdfs = dbMean.Select(x => new ExponentialDistributionCdf(Util.ToLinear(x))).ToArray();
        }

        public double[] GetChannelRates()
        {
            return _cdfs
                .Select(cdf => cdf.GetInverse(_gen.NextDouble()))
                .Select(linearSnr => Math.Max(linearSnr, 0))
                .Select(linearSnr => BitsPerSymbol.GetBitsPerSymbol(linearSnr) * _symbolsPerSchedulingInterval)
                .ToArray();
        }
    }
}
