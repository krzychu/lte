﻿using Lte.Core.Scheduler.Uwr;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Test.Core.Scheduler.Uwr
{
    [TestClass]
    public class PotentialTest
    {
        [TestMethod]
        public void CalculatesValueCorrectly()
        {
            Assert.AreEqual(1.0 / 2.0 + 1.0 / 4.0 + 1.0 / 8.0, Potential.GetValue(new[] { 1.0, 2.0, 4.0 }));
        }

        [TestMethod]
        public void CalculatesDerivativesCorrectly()
        {
            var derivatives = Potential.GetDerivatives(new[] { 1.0, 2.0, 4.0 });

            Assert.AreEqual(-(0.5 + 0.25), derivatives[0]);
            Assert.AreEqual(-0.25 * (1 + 0.25), derivatives[1]);
            Assert.AreEqual(-0.25 * 0.25 * (1 + 0.5), derivatives[2]);
        }
    }
}
