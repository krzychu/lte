﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.MaxRate
{
    public class MaxRateScheduler : IScheduler
    {
        private int _userCount;

        public MaxRateScheduler(int userCount)
        {
            _userCount = userCount;
        }

        public int SelectActiveUser(double[] channelRates)
        {
            Debug.Assert(channelRates.Length == _userCount);

            return Util.ArgMax(channelRates);
        }
    }
}
