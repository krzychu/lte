Software Requirements
=====================

In order to use this simulator you need:

* Windows 7 or above
* Visual Studio 2013 or above
* Git client, for example excellent [Git Extensions](https://code.google.com/p/gitextensions/)
* Any version of SQL Server 2012 


Running Simulations
===================

1. Clone this repository
1. Configure your database - make sure that login with name 'test', password 'test' and sysadmin role exists
1. Compile solution - just open the Lte.sln file and press F5. First compilation might take some time, because VS will obtain required NuGet packages from the internet.
1. Run simulations - make sure that project "Paper" is selected as a startup project and press F5. By editing main method you can choose what action will be performed - simulator can either generate data or show experiment report.
