﻿using Lte.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core
{
    public class SimulationContext
    {
        public int Seed { get; set; }
        public int Time { get; set; }
        public int UserCount { get; set; }
        public int Duration { get; set; }
    }
}
