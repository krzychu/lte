﻿using Lte.Core.Scheduler.Uwr.StateUpdater;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr.UserSelector
{
    public class MaxRateUserSelector : IUserSelector
    {
        public int SelectActiveUser(Constants constants, State currentState, IStateUpdater stateUpdater, double[] channelRates)
        {
            /*
            var feasible = constants.Users
                .Where(user => Potential.IsFeasible(stateUpdater.GetNextState(constants, currentState, channelRates, user)))
                .ToArray();
            */

            var positions = constants.Users
                .Select(x => Potential.GetPosition(constants, stateUpdater.GetNextState(constants, currentState, channelRates, x)))
                .ToArray();

            var isFeasible = positions
                .Select(x => Potential.IsFeasible(constants, x))
                .ToArray();

            return constants.Users
                .Select(user => isFeasible[user] ? channelRates[user] : double.MinValue).ArgMax();
           
        }
    }
}
