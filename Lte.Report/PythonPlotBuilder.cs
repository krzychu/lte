﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lte.Report
{
    public class PythonPlotBuilder : IPlotBuilder, IDisposable
    {
        private TextWriter _scriptWriter;
        private TextWriter _dataWriter;
        private string _dataPath;
        private StringBuilder _scriptBuilder = new StringBuilder();
        private Regex _fillRx = new Regex(@"__(?<name>\w+)__");

        public PythonPlotBuilder(string scriptPath, string dataPath)
        {
            _dataPath = dataPath;
            _scriptWriter = new StreamWriter(new FileStream(scriptPath, FileMode.Create, FileAccess.Write));
            _dataWriter = new StreamWriter(new FileStream(dataPath, FileMode.Create, FileAccess.Write));
        }

        public void Plot(double[] xs, double[] ys, PlotStyle style, Color color, string title)
        {
            title = title.Trim();
            _dataWriter.WriteLine("{0}, xs, {1}", title, string.Join(", ", xs.Select(x => x.ToString(CultureInfo.InvariantCulture))));
            _dataWriter.WriteLine("{0}, ys, {1}", title, string.Join(", ", ys.Select(x => x.ToString(CultureInfo.InvariantCulture))));
        
            var colorHex = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");

            if(style == PlotStyle.Line)
                _scriptBuilder.AppendFormat("plt.plot(data['{0}']['xs'], data['{0}']['ys'], color = '{1}', linestyle = 'solid', label='{0}')", title, colorHex);
            else
                _scriptBuilder.AppendFormat("plt.plot(data['{0}']['xs'], data['{0}']['ys'], color = '{1}', marker = 'D', linestyle='none', markevery={2}, label='{0}')", 
                    title, colorHex, xs.Length / 100);

            _scriptBuilder.AppendLine();
        }

        public void Plot(double[] ys, PlotStyle style, Color color, string title)
        {
            Plot(Enumerable.Range(0, ys.Length).Select(x => (double)x).ToArray(), ys, style, color, title);
        }

        public string XLabel { get; set; }
        public string YLabel { get; set; }

        public void Dispose()
        {
            string scriptTemplate = null;
            using(var reader = new StreamReader(GetType().Assembly.GetManifestResourceStream("Lte.Report.plot_template.py")))
                scriptTemplate = reader.ReadToEnd();

            var content = new Dictionary<string, string>()
            {
                {"data_path", _dataPath},
                {"xlabel", XLabel ?? ""},
                {"ylabel", YLabel ?? ""},
                {"plot", _scriptBuilder.ToString()}
            };

            _scriptWriter.Write(_fillRx.Replace(scriptTemplate, (Match m) => content[m.Groups["name"].Value]));
            _scriptWriter.Dispose();
            _dataWriter.Dispose();
        }
    }
}
