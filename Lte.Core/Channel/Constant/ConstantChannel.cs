﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.Constant
{
    public class ConstantChannel : IChannel
    {
        private double[] _rates;

        public ConstantChannel(double[] rates)
        {
            _rates = rates;
        }

        public double[] GetChannelRates()
        {
            return _rates;
        }

        public double MaxRate
        {
            get { return _rates.Max(); }
        }
    }
}
