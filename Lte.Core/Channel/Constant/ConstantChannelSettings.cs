﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.Constant
{
    public class ConstantChannelSettings : IChannelSettings
    {
        public double[] Rates { get; set; }

        public IChannel GetChannel(SimulationContext context)
        {
            return new ConstantChannel(Rates);
        }

        public string Name
        {
            get { return "Constant"; }
        }

        public double MaxBitsPerSchedulingInterval
        {
            get 
            {
                return Rates.Max();
            }
        }
    }   
}
