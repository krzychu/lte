﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.EncodedRayleigh
{
    public class EncodedRayleighSettings : IChannelSettings
    {
        public string Name
        {
            get { return "Rayleigh"; }
        }

        public double[] DbMean { get; set; }
        public double SymbolsPerSchedulingInterval { get; set; }

        public IChannel GetChannel(SimulationContext context)
        {
            return new EncodedRayleighChannel(SymbolsPerSchedulingInterval, DbMean, context.Seed);
        }

        public static EncodedRayleighSettings FromUserClasses(int symbolsPerScheduledInterval, int poorCount, int averageCount, int goodCount)
        {
            var poor = Enumerable.Repeat(EncodedRayleighChannel.UserClasses.PoorSnrDb, poorCount);
            var average = Enumerable.Repeat(EncodedRayleighChannel.UserClasses.AverageSnrDb, averageCount);
            var good = Enumerable.Repeat(EncodedRayleighChannel.UserClasses.GoodSnrDb, goodCount);

            return new EncodedRayleighSettings()
            {
                DbMean = poor.Concat(average).Concat(good).ToArray(),
                SymbolsPerSchedulingInterval = symbolsPerScheduledInterval
            };
        }

        public double MaxBitsPerSchedulingInterval
        {
            get
            {
                return 4.8 * SymbolsPerSchedulingInterval;
            }
        }
    }
}
