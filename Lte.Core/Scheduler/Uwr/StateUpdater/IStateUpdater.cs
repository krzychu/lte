﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Scheduler.Uwr.StateUpdater
{
    public interface IStateUpdater
    {
        State GetNextState(Constants constants, State current, double[] channelRates, int selected);
    }
}
