﻿using Lte.Core;
using Lte.Report;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.ReportTest
{
    public class LineTest : IPlot
    {
        public void Draw(IPlotBuilder plotter, IStorage storage)
        {
            var xs = Enumerable.Range(0, 10).Select(x => (double)x).ToArray();
            var ys = xs.Select(x => Math.Exp(x)).ToArray();
            plotter.XLabel = "Megatons";
            plotter.YLabel = "Awesomeness";

            plotter.Plot(xs, ys, PlotStyle.Line, Color.Red, "undisputed");
        }

        public override string ToString()
        {
            return "Line Test";
        }
    }
}
