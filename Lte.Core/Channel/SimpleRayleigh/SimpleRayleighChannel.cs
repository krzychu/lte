﻿using Lte.Core.Channel.EncodedRayleigh;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel.SimpleRayleigh
{
    public class SimpleRayleighChannel : IChannel
    {
        private ExponentialDistributionCdf[] _cdfs;
        private Random _generator;
        private double _bandwidthHertz;
        private double _schedulingIntervalSeconds;
        private double _maxBitsPerInterval;
        
        public SimpleRayleighChannel(double[] dbMeans, double schedulingIntervalSeconds, double bandwidthHertz, double maxBitsPerInterval, int seed)
        {
            _generator = new Random(seed);
            _cdfs = dbMeans
                .Select(x => new ExponentialDistributionCdf(Util.ToLinear(x)))
                .ToArray();

            _bandwidthHertz = bandwidthHertz;
            _schedulingIntervalSeconds = schedulingIntervalSeconds;
            _maxBitsPerInterval = maxBitsPerInterval;
        }

        private double GetChannelRate(double linearSnr)
        {
            var fromDistribution = Shannon.GetBitsPerSecond(_bandwidthHertz, linearSnr) * _schedulingIntervalSeconds;
            return Math.Min(fromDistribution, _maxBitsPerInterval);
        }

        public double[] GetChannelRates()
        {
            return _cdfs
                .Select(cdf => cdf.GetInverse(_generator.NextDouble()))
                .Select(linearSnr => GetChannelRate(linearSnr))
                .ToArray();
        }
    }
}
