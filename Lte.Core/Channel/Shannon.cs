﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lte.Core.Channel
{
    public static class Shannon
    {
        public static double GetBitsPerSecond(double bandwidthHz, double linearSnr)
        {
            return bandwidthHz * Util.Log2(1 + linearSnr);
        }

        public static double GetLinearSnr(double bandwidthHertz, double bitsPerSecond)
        {
            return Math.Pow(2, bitsPerSecond / bandwidthHertz) - 1;
        }
    }
}
