﻿using Lte.Core;
using Lte.Core.Channel;
using Lte.Core.Channel.EncodedRayleigh;
using Lte.Core.Channel.SimpleRayleigh;
using Lte.Core.Model;
using Lte.Core.Postprocessing;
using Lte.Core.Scheduler;
using Lte.Core.Scheduler.MaxRate;
using Lte.Core.Scheduler.ProportionalFair;
using Lte.Core.Scheduler.RoundRobin;
using Lte.Core.Scheduler.Uwr;
using Lte.Core.Scheduler.Uwr.StateUpdater;
using Lte.Core.Scheduler.Uwr.UserSelector;
using Lte.Persistence;
using Lte.Report;
using Paper.Plots;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paper
{
    class Paper
    {
        private const string connectionString = @"Server=(local)\SQLEXPRESS; Initial Catalog=Paper; Integrated Security=True";

        //[STAThread]
        static void Main(string[] args)
        {
            Simulate();
            //Report();
        }

        private static void Report()
        {
            double delta = 1.0 / 4.0;

            int simpleShift = 0;
            double simpleA = 2.51e7;

            int encodedShift = 4;
            double encodedA = 7257.6;  

            Func<int, ForSimulation[]> classic = (shift) => new[] 
            {
                new ForSimulation() {SimulationId = 1 + shift, Label = "Proportional Fair", Color = Color.Purple},
                new ForSimulation() {SimulationId = 3 + shift, Label = "Classic UWR", Color = Color.Blue},
                new ForSimulation() {SimulationId = 4 + shift, Label = "Max Rate", Color = Color.Red},
            };

            Func<int, ForSimulation[]> maxRate = (shift) => new[] {
                new ForSimulation() {SimulationId = 1 + shift, Label = "Proportional Fair", Color = Color.Purple},
                new ForSimulation() {SimulationId = 2 + shift, Label = "Max Rate UWR", Color = Color.Green},
                new ForSimulation() {SimulationId = 4 + shift, Label = "Max Rate", Color = Color.Red},
            };

            Func<int, ForSimulation[]> uwrs = (shift) => new[] {
                new ForSimulation() {SimulationId = 2 + shift, Label = "Max Rate UWR", Color = Color.Green},
                new ForSimulation() {SimulationId = 3 + shift, Label = "Classic UWR", Color = Color.Blue},
            };

            ReportApp.Run(new IPlot[] 
            {
                new ChannelRatePlot("Bits per scheduling interval (channel 1)", 1 + encodedShift),
                
                new TotalVsPossible("Total vs Possible for Classic UWR (channel 1)", delta, -encodedA, classic(encodedShift)),
                new TotalVsPossible("Total vs Possible for Max Rate UWR (channel 1)", delta, -encodedA, maxRate(encodedShift)),
                new TotalVsPossible("Total vs Possible for Variants of UWR (channel 1)", delta, -encodedA, uwrs(encodedShift)),
                
                new RoundEfficiencyPlot("Round Efficiency for Classic UWR (channel 1)", classic(encodedShift)),
                new RoundEfficiencyPlot("Round Efficiency for Max Rate UWR (channel 1)", maxRate(encodedShift)),

                new WelfarePlot("Welfare for Classic UWR (channel 1)", classic(encodedShift)),
                new WelfarePlot("Welfare for Max Rate UWR (channel 1)", maxRate(encodedShift)),



                new ChannelRatePlot("Bits per scheduling interval (channel 2)", 1 + simpleShift),
                
                new TotalVsPossible("Total vs Possible for Classic UWR (channel 2)", delta, -simpleA, classic(simpleShift)),
                new TotalVsPossible("Total vs Possible for Max Rate UWR (channel 2)", delta, -simpleA, maxRate(simpleShift)),
                new TotalVsPossible("Total vs Possible for Variants of UWR (channel 2)", delta, -simpleA, uwrs(simpleShift)),
                
                new RoundEfficiencyPlot("Round Efficiency for Classic UWR (channel 2)", classic(simpleShift)),
                new RoundEfficiencyPlot("Round Efficiency for Max Rate UWR (channel 2)", maxRate(simpleShift)),

                new WelfarePlot("Welfare for Classic UWR (channel 2)", classic(simpleShift)),
                new WelfarePlot("Welfare for Max Rate UWR (channel 2)", maxRate(simpleShift))
            },

            new SqlStorage(connectionString, false),              
            "");
        }

        private static void Simulate()
        {
            var channelSettings = new IChannelSettings[] 
            {
                SimpleRayleighSettings.FromUserClasses(20e6, 1e-3, 35, 1, 1, 1),
                EncodedRayleighSettings.FromUserClasses(14, 1, 1, 1)
            };

            var schedulerSettings = new ISchedulerSettings[] 
            {
                new ProportionalFairSettings() {Tau = 0.03},               

                new UwrSchedulerSettings() 
                { 
                    StateUpdater = new DecayingStateUpdater(),
                    UserSelector = new MaxRateUserSelector()
                },

                new UwrSchedulerSettings()
                {
                    StateUpdater = new DecayingStateUpdater(),
                    UserSelector = new ClassicUserSelector()
                },
 
                new MaxRateSettings()
            };


            var storage = new SqlStorage(connectionString, true);
            var runner = new SimulationRunner(storage);

            foreach (var channel in channelSettings)
            {
                foreach (var scheduler in schedulerSettings)
                {
                    Console.WriteLine("==================================================");
                    Console.WriteLine(channel.Name);
                    Console.WriteLine(scheduler.Name);

                    var uwr = scheduler as UwrSchedulerSettings;
                    if (uwr != null)
                        uwr.MaxChannelCapacity = channel.MaxBitsPerSchedulingInterval;

                    var simSettings = new SimulationSettings()
                    {
                        ChannelSettings = channel,
                        SchedulerSettings = scheduler,
                        UserCount = 3,
                        Duration = 10000,
                        RepetitionCount = 10
                    };

                    runner.Run(simSettings);
                }
            }

            Console.WriteLine("Postprocessing");
            var post = new PostprocessingRunner();
            post.AddStage(new SumOfChannelRate());
            post.AddStage(new SumOfTransmissions());
            post.AddStage(new Welfare());
            post.Run(storage);
            Console.WriteLine("Done");
        }
    }
}
